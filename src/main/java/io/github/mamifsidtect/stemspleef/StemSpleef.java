package io.github.mamifsidtect.stemspleef;

import io.github.mamifsidtect.stemspleef.listeners.AbilityListener;
import io.github.mamifsidtect.stemspleef.listeners.AbilitySelectListener;
import io.github.mamifsidtect.stemspleef.listeners.GameListener;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.CommandManager;
import io.github.mamifsidtect.stemspleef.managers.ItemManager;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class StemSpleef extends JavaPlugin {
	
	public static Permission perms = null;
	public static Economy econ = null;
	
	public static ItemManager im = new ItemManager();
	
	public static Inventory abilityMenu, arenaMenu;
	
	@Override
	public void onEnable() {
		PluginManager pm = Bukkit.getServer().getPluginManager();
	
		pm.registerEvents(new GameListener(), this);
		pm.registerEvents(new AbilitySelectListener(), this);
		pm.registerEvents(new AbilityListener(), this);
		
		abilityMenu = Bukkit.getServer().createInventory(null, 9, ChatColor.RED + "Select your ability"); {
			abilityMenu.setItem(0, new ItemStack(im.creeperWand));
			abilityMenu.setItem(2, new ItemStack(im.fireWand));
			abilityMenu.setItem(4, new ItemStack(im.iceWand));
			abilityMenu.setItem(6, new ItemStack(im.mageWand));
			abilityMenu.setItem(8, new ItemStack(im.snowWand));
		}
		
		arenaMenu = Bukkit.getServer().createInventory(null, 9, ChatColor.RED + "Select an arena"); {
			
		}
				
		ArenaManager.getInstance().setupArenas();
		
		CommandManager cm = new CommandManager();
		cm.setup();
		getCommand("spleef").setExecutor(cm);
		
		setupPermissions();
		setupEconomy();
		
		getLogger().info("StemSpleef V2 is now enabled!");
	}
	
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
    
    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            econ = economyProvider.getProvider();
        }

        return (econ != null);
    }
	
	public static Plugin getPlugin() {
		return Bukkit.getServer().getPluginManager().getPlugin("StemSpleef");
	}
}
