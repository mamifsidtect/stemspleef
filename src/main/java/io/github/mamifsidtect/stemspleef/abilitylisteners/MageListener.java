package io.github.mamifsidtect.stemspleef.abilitylisteners;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.event.CustomProjectileHitEvent;
import io.github.mamifsidtect.stemspleef.event.CustomProjectileHitEvent.HitType;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.ItemManager;
import io.github.mamifsidtect.stemspleef.managers.Particles;
import io.github.mamifsidtect.stemspleef.projectile.CustomProjectile;
import io.github.mamifsidtect.stemspleef.projectile.ItemProjectile;
import io.github.mamifsidtect.stemspleef.tools.TypedRunnable;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class MageListener implements Listener {

	public static ItemManager im = new ItemManager();
	public static ArrayList<String> mageCooldown = new ArrayList<String>();
	
	public boolean isInCooldown(Player p) {
		if (mageCooldown.contains(p.getName())) {
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void onMageUse(PlayerInteractEvent event) {
		final Player p = event.getPlayer();
		Action a = event.getAction();
		if (ArenaManager.getInstance().getArena(p) != null) {
			if (p.getItemInHand().equals(im.mageWand)) {
				if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
					if (!isInCooldown(p)) {
						
						mageCooldown.add(p.getName());
						
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(StemSpleef.getPlugin(), new Runnable() {
							public void run() {
								mageCooldown.remove(p.getName());
							}
						}, 1200);
						
						CustomProjectile proj = new ItemProjectile("mageEnderpearl1", p, new ItemStack(Material.ENDER_PEARL), 1.5F);
						proj.addTypedRunnable(new TypedRunnable<ItemProjectile>() {
							@Override
							public void run(ItemProjectile o) {
								Particles.LARGE_SMOKE.display(o.getEntity().getLocation(), 0, 0, 0, 0.5F, 5);
								Particles.FLAME.display(o.getEntity().getLocation(), 0, 0, 0, 0.3F, 4);
							}
						});
						CustomProjectile proj2 = new ItemProjectile("mageEnderpearl1", p, new ItemStack(Material.ENDER_PEARL), 2F);
						proj2.addTypedRunnable(new TypedRunnable<ItemProjectile>() {
							@Override
							public void run(ItemProjectile o) {
								Particles.LARGE_SMOKE.display(o.getEntity().getLocation(), 0, 0, 0, 0.5F, 5);
								Particles.FLAME.display(o.getEntity().getLocation(), 0, 0, 0, 0.3F, 4);
							}
						});
						CustomProjectile proj3 = new ItemProjectile("mageEnderpearl1", p, new ItemStack(Material.ENDER_PEARL), 1.3F);
						proj3.addTypedRunnable(new TypedRunnable<ItemProjectile>() {
							@Override
							public void run(ItemProjectile o) {
								Particles.LARGE_SMOKE.display(o.getEntity().getLocation(), 0, 0, 0, 0.5F, 5);
								Particles.FLAME.display(o.getEntity().getLocation(), 0, 0, 0, 0.3F, 4);
							}
						});
						CustomProjectile proj4 = new ItemProjectile("mageEnderpearl1", p, new ItemStack(Material.ENDER_PEARL), 1.4F);
						proj4.addTypedRunnable(new TypedRunnable<ItemProjectile>() {
							@Override
							public void run(ItemProjectile o) {
								Particles.LARGE_SMOKE.display(o.getEntity().getLocation(), 0, 0, 0, 0.5F, 5);
								Particles.FLAME.display(o.getEntity().getLocation(), 0, 0, 0, 0.3F, 4);
							}
						});
						CustomProjectile proj5 = new ItemProjectile("mageEnderpearl1", p, new ItemStack(Material.ENDER_PEARL), 1.1F);
						proj5.addTypedRunnable(new TypedRunnable<ItemProjectile>() {
							@Override
							public void run(ItemProjectile o) {
								Particles.LARGE_SMOKE.display(o.getEntity().getLocation(), 0, 0, 0, 0.5F, 5);
								Particles.FLAME.display(o.getEntity().getLocation(), 0, 0, 0, 0.3F, 4);
							}
						});
					} else {
						p.sendMessage("You cannot yet use your ability again!");
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onSnowHit(CustomProjectileHitEvent event) {
		if (event.getHitType() == HitType.BLOCK) {
			if (event.getHitBlock().getType() == Material.SNOW_BLOCK) {
				if (event.getProjectile().getProjectileName().contains("mageEnderpearl")) {
					event.getHitBlock().setType(Material.AIR);
					event.getProjectile().getEntity().remove();
					Particles.CLOUD.display(event.getHitBlock().getLocation(), 0, 0, 0, 0, 2);
				}
			}
		}
	}
}
