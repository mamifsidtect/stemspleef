package io.github.mamifsidtect.stemspleef.managers;

import io.github.mamifsidtect.stemspleef.commands.Create;
import io.github.mamifsidtect.stemspleef.commands.Delete;
import io.github.mamifsidtect.stemspleef.commands.ForceStart;
import io.github.mamifsidtect.stemspleef.commands.ForceStop;
import io.github.mamifsidtect.stemspleef.commands.Join;
import io.github.mamifsidtect.stemspleef.commands.Leave;
import io.github.mamifsidtect.stemspleef.commands.MagicCommands;
import io.github.mamifsidtect.stemspleef.commands.Reload;
import io.github.mamifsidtect.stemspleef.commands.SetGameSpawns;
import io.github.mamifsidtect.stemspleef.commands.SetLocation;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {
	
	private ArrayList<MagicCommands> cmds = new ArrayList<MagicCommands>();

	public void setup() {
		cmds.add(new Create());
		cmds.add(new Delete());
		cmds.add(new ForceStart());
		cmds.add(new ForceStop());
		cmds.add(new Join());
		cmds.add(new Leave());
		cmds.add(new Reload());
		cmds.add(new SetLocation());
		cmds.add(new SetGameSpawns());
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!(sender instanceof Player)) {
			MessageManager.getInstance().msg(sender, MessageType.BAD, "Only players can use StemSpleef!");
			return true;
		}

		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("spleef")) {
			if (args.length == 0) {
				for (MagicCommands mc : cmds) MessageManager.getInstance().msg(p, MessageType.INFO, "/spleef " + aliases(mc) + " " + mc.getUsage() + " - " + mc.getMessage());
				return true;
			}

			MagicCommands c = getCommand(args[0]);

			if (c == null) {
				MessageManager.getInstance().msg(sender, MessageType.BAD, "That command doesn't exist!");
				return true;
			}

			Vector<String> a = new Vector<String>(Arrays.asList(args));
			a.remove(0);
			args = a.toArray(new String[a.size()]);

			c.onCommand(p, args);

			return true;
		}
		return true;
	}

	private String aliases(MagicCommands cmd) {
		String fin = "";

		for (String a : cmd.getAliases()) {
			fin += a + " | ";
		}

		return fin.substring(0, fin.lastIndexOf(" | "));
	}

	private MagicCommands getCommand(String name) {
		for (MagicCommands cmd : cmds) {
			if (cmd.getClass().getSimpleName().equalsIgnoreCase(name)) return cmd;
			for (String alias : cmd.getAliases()) if (name.equalsIgnoreCase(alias)) return cmd;
		}
		return null;
	}
}