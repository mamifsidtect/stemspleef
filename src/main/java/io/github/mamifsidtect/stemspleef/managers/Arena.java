package io.github.mamifsidtect.stemspleef.managers;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.listeners.AbilityListener;
import io.github.mamifsidtect.stemspleef.listeners.GameListener;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Arena {
	
	public enum ArenaState { DISABLED, WAITING, COUNTING_DOWN, SPLEEFING, FINISHING; }
	
	public int id, taskID, numPlayers, currentPlayers, minPlayers = 0;
	protected ArenaState state = ArenaState.DISABLED;
	public ArrayList<PlayerData> data;
	public Location spawnPoint;
	
	private static ItemManager im = new ItemManager();
	
	public Arena(int id) {
		this.id = id;
		this.data = new ArrayList<PlayerData>();
		this.numPlayers = SettingsManager.getArenas().get("arenas." + id + ".numPlayers");
		this.minPlayers = SettingsManager.getArenas().get("arenas." + id + ".minPlayers");
		
		ConfigurationSection s = SettingsManager.getArenas().get("arenas." + id + ".spawn");
		
		spawnPoint = LocationManager.locationFromConfig(s, true);

		state = ArenaState.WAITING;
	}
	
	public int getID() {
		return id;
	}
	
	public Location getSpawnPoint() {
		return spawnPoint;
	}
	
	public ArenaState getState() {
		return state;
	}

	public int getCurrentPlayers() {
		return currentPlayers;
	}
	
	public void addPlayer(Player p) {
		if (currentPlayers >= numPlayers) {
			MessageManager.getInstance().msg(p, MessageType.BAD, "This is arena is full!");
			return;
		}
		
		if (spawnPoint == null) {
			MessageManager.getInstance().msg(p, MessageType.BAD, "There is no spawn point for this arena yet!");
			return;
		}
		
		currentPlayers++;
		
		data.add(new PlayerData(p));
		
		p.getInventory().clear();
		
		p.setGameMode(GameMode.SURVIVAL);
		
		p.teleport(spawnPoint);
		
		p.setFoodLevel(20);
		p.setHealth(20);
		
		p.getInventory().setItem(0, new ItemStack(Material.DIAMOND_SPADE));
		p.getInventory().setItem(8, new ItemStack(im.classWand));
		
		GameListener.getInstance().getBlocksBroken().put(p.getName(), new Integer(0));
		
		sendMessage(MessageType.INFO, p.getName() + " has joined the arena! (" + currentPlayers + "/" + numPlayers + ")");
		
		for (PlayerData pd : data) updateScoreboard(pd.getPlayer());
		
		if (currentPlayers == minPlayers) {
			start();
		}
	}
	
	public void removePlayer(Player p) {
		PlayerData d = getPlayerData(p);
		d.restorePlayer();
		data.remove(d);
		
		if (currentPlayers != 0) {
			for (PlayerData pd : data) updateScoreboard(pd.getPlayer());
		} else {
			stop();
		}
		
		currentPlayers--;
	}
	
	public void playerLeave(Player p) {	
		currentPlayers--;
		
		if (currentPlayers >= 2) {
			for (PlayerData pd : data) updateScoreboard(pd.getPlayer());

		} else if (currentPlayers <= 1) {
			if (state == ArenaState.SPLEEFING) {
				stop(data.get(0).getPlayer());
			} else {
				PlayerData lastPlayer = getPlayerData(data.get(0).getPlayer());
				lastPlayer.restorePlayer();
				data.remove(lastPlayer);
				
				state = ArenaState.WAITING;
			}
		}
	}
	
	public void updateScoreboard(Player p) {
		
		SimpleScoreboard ss = new SimpleScoreboard(ChatColor.GOLD + "Stem " + ChatColor.GREEN + "Spleef");
		ss.add(ChatColor.BLUE + "" + ChatColor.BOLD + ">=+=+=+=+=+<=>+=+=+=+=+<");
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "" + ChatColor.BOLD + "Your Ability:§r");
		if (AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
			ss.add("" + AbilityListener.getInstance().getPlayerAblility().get(p.getName()));
		} else {
			ss.add("None");
		}		
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "" + ChatColor.BOLD + "Blocks You've Broken:");
		ss.add("" + GameListener.getInstance().getBlocksBroken().get(p.getName()));
		ss.blankLine();
		
		ss.add(ChatColor.YELLOW + "" + ChatColor.BOLD + "Players Left:");
		ss.add("" + currentPlayers);
		ss.blankLine();
		
		ss.add(ChatColor.BLUE + "" + ChatColor.BOLD + ">=+=+=+=+=+<=>+=+=+=+=+<");
		
		ss.build();
		ss.send(p);
	}
	
	public void start() {
		final Iterator<PlayerData> playerIterator = data.iterator();
		
		final Countdown c = new Countdown(60, "Get ready to spleef in %t!", "beginning", this, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown gameEnd = new Countdown(900, "Spleefing ends in %t", "ended", this, 900, 840, 780, 720, 660, 600, 540, 480, 420, 360, 300, 240, 180, 120, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		
		final String creeperClass = AbilityList.getInstance().getCreeperClass();
		final String fireClass = AbilityList.getInstance().getFireClass();
		final String iceClass = AbilityList.getInstance().getIceClass();
		final String mageClass = AbilityList.getInstance().getMageClass();
		final String snowClass = AbilityList.getInstance().getSnowClass();		
		
		taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(StemSpleef.getPlugin(), new Runnable() {
			public void run() {
				if (currentPlayers >= minPlayers) {
					if (!c.isDone()) {
						c.run();
						
						state = ArenaState.COUNTING_DOWN;
					} else {
						Bukkit.getServer().getScheduler().cancelTask(taskID);
						
						while (playerIterator.hasNext()) {
							for (PlayerData pd : data) {
								if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(creeperClass)) {
									ItemManager.getInstance().giveCreeperWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(fireClass)) {
									ItemManager.getInstance().giveFireWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(iceClass)) {
									ItemManager.getInstance().giveIceWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(mageClass)) {
									ItemManager.getInstance().giveMageWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(snowClass)) {
									ItemManager.getInstance().giveSnowWand(pd.getPlayer(), 8);
								} else if (!AbilityListener.getInstance().getPlayerAblility().containsKey(pd.getPlayer())) {
									ItemManager.getInstance().giveCreeperWand(pd.getPlayer(), 8);
									AbilityListener.getInstance().getPlayerAblility().put(pd.getPlayer().getName(), creeperClass);
								}
							}
						}
						
						state = ArenaState.SPLEEFING;
						
						taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(StemSpleef.getPlugin(), new Runnable() {
							public void run() {
								if (!(currentPlayers <= minPlayers)) {
									if (!gameEnd.isDone()) {
										gameEnd.run();
										
										state = ArenaState.SPLEEFING;
									} else {
										stop(data.get(0).getPlayer());
										
										StemSpleef.getPlugin().getLogger().info("Arena " + id + " has ended!");
									}
								} else {
									Bukkit.getServer().getScheduler().cancelTask(taskID);
									
									stop();
								}
							}
						}, 0, 20);
					}
				} else {
					Bukkit.getServer().getScheduler().cancelTask(taskID);
					
					state = ArenaState.WAITING;
				}
			}
		}, 0, 20);
	}
	
	public void forceStart() {
		final Iterator<PlayerData> playerIterator = data.iterator();
		
		final Countdown c = new Countdown(60, "Get ready to spleef in %t!", "beginning", this, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		final Countdown gameEnd = new Countdown(900, "Spleefing ends in %t", "ended", this, 900, 840, 780, 720, 660, 600, 540, 480, 420, 360, 300, 240, 180, 120, 60, 30, 20, 10, 5, 4, 3, 2, 1);
		
		final String creeperClass = AbilityList.getInstance().getCreeperClass();
		final String fireClass = AbilityList.getInstance().getFireClass();
		final String iceClass = AbilityList.getInstance().getIceClass();
		final String mageClass = AbilityList.getInstance().getMageClass();
		final String snowClass = AbilityList.getInstance().getSnowClass();
		
		taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(StemSpleef.getPlugin(), new Runnable() {
			public void run() {
				if (currentPlayers >= 1) {
					if (!c.isDone()) {
						c.run();
						
						state = ArenaState.COUNTING_DOWN;
					} else {
						Bukkit.getServer().getScheduler().cancelTask(taskID);
						
						while (playerIterator.hasNext()) {
							for (PlayerData pd : data) {
								 if (!AbilityListener.getInstance().getPlayerAblility().containsKey(pd.getPlayer())) {
									ItemManager.getInstance().giveCreeperWand(pd.getPlayer(), 8);
									AbilityListener.getInstance().getPlayerAblility().put(pd.getPlayer().getName(), creeperClass);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(creeperClass)) {
									ItemManager.getInstance().giveCreeperWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(fireClass)) {
									ItemManager.getInstance().giveFireWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(iceClass)) {
									ItemManager.getInstance().giveIceWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(mageClass)) {
									ItemManager.getInstance().giveMageWand(pd.getPlayer(), 8);
								} else if (AbilityListener.getInstance().getPlayerAblility().get(pd.getPlayer().getName()).equals(snowClass)) {
									ItemManager.getInstance().giveSnowWand(pd.getPlayer(), 8);
								}
							}
						}
						
						state = ArenaState.SPLEEFING;
						
						taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(StemSpleef.getPlugin(), new Runnable() {
							public void run() {
								if (currentPlayers != 0) {
									if (!gameEnd.isDone()) {
										gameEnd.run();
										
										state = ArenaState.SPLEEFING;
									} else {
										stop(data.get(0).getPlayer());
										
										StemSpleef.getPlugin().getLogger().info("Arena " + id + " has ended!");
									}
								} else {
									Bukkit.getServer().getScheduler().cancelTask(taskID);
									
									stop();
								}
							}
						}, 0, 20);
					}
				} else {
					Bukkit.getServer().getScheduler().cancelTask(taskID);
					
					state = ArenaState.WAITING;
				}
			}
		}, 0, 20);
	}
	
	@SuppressWarnings("deprecation")
	public void stop(Player winner) {
		Iterator<PlayerData> playerIterator = data.iterator();
		
		sendMessage(MessageType.GOOD, winner.getName() + " has won the game!");
		
		StemSpleef.econ.depositPlayer(winner.getName(), 10);
		
		while (playerIterator.hasNext()) {
			for (PlayerData pd : data) {
				removePlayer(pd.getPlayer());
			}
		}
		
		state = ArenaState.WAITING;
	}
	
	public void stop() {
		Iterator<PlayerData> playerIterator = data.iterator();
		
		while (playerIterator.hasNext()) {
			for (PlayerData pd : data) {
				removePlayer(pd.getPlayer());
			}
		}
		
		state = ArenaState.WAITING;
	}
	
	public boolean containsPlayer(Player p) {
		return getPlayerData(p) != null;
	}

	public PlayerData getPlayerData(Player p) {
		for (PlayerData d : data) {
			if (d.isForPlayer(p)) return d;
		}
		return null;
	}
	
	public void sendMessage(MessageType type, String... messages) {
		for (PlayerData d : data) MessageManager.getInstance().msg(d.getPlayer(), type, messages);
	}
}
	
	class PlayerData {
		
		private String playerName;
		private ItemStack[] contents, armorContents;
		private GameMode gm;
		private Location spawn;

		protected PlayerData(Player p) {
			this.playerName = p.getName();
			this.contents = p.getInventory().getContents();
			this.armorContents = p.getInventory().getArmorContents();
			this.gm = p.getGameMode();
			this.spawn = Bukkit.getWorld("world").getSpawnLocation();
		}
		
		@SuppressWarnings("deprecation")
		protected Player getPlayer() {
			return Bukkit.getServer().getPlayer(playerName);
		}

		@SuppressWarnings("deprecation")
		protected void restorePlayer() {
			Player p = Bukkit.getServer().getPlayer(playerName);
			
			p.getInventory().setContents(contents);
			p.getInventory().setArmorContents(armorContents);
			p.setGameMode(gm);
			p.teleport(spawn);
			p.setScoreboard(Bukkit.getServer().getScoreboardManager().getNewScoreboard());
		}

		protected boolean isForPlayer(Player p) {
			return playerName.equalsIgnoreCase(p.getName());
		}
	}
	
class Countdown implements Runnable {

	private boolean isDone = false;
	private int timer;
	private String msg, type;
	private Arena a;
	private ArrayList<Integer> countingNums;

	public Countdown(int start, String msg, String type, Arena a, int... countingNums) {
		this.timer = start;
		this.msg = msg;
		this.type = type;
		this.a = a;
		this.countingNums = new ArrayList<Integer>();
		for (int i : countingNums) this.countingNums.add(i);
		}

	public Countdown(int i, String string, Runnable runnable, int j, int k,
		int l, int m, int n, int o, int p, int q, int r, int s) {
			
		}

	public void run() {
		if (timer == 0) {
			a.sendMessage(MessageType.GOOD, "The game is now " + type);
			isDone = true;
			return;
		}
		
		if (countingNums.contains(timer)) {
			
			if (timer >= 61) {
				a.sendMessage(MessageType.INFO, msg.replaceAll("%t", timer/60 + " minutes"));
			} else if (timer == 60) {
				a.sendMessage(MessageType.INFO, msg.replaceAll("%t", timer/60 + " minute"));
			} else if (timer <= 59) {
				a.sendMessage(MessageType.INFO, msg.replaceAll("%t", timer + " seconds"));
			} else if (timer == 1) {
				a.sendMessage(MessageType.INFO, msg.replaceAll("%t", timer + " second"));
			}
			
		}
			timer--;
		}

	public boolean isDone() {
		return isDone;
	}
}