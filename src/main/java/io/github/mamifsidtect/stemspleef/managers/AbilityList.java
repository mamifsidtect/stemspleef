package io.github.mamifsidtect.stemspleef.managers;

public class AbilityList {

	/**
	 * Instantiates a new instance of this class 
	 */
	private static AbilityList instance = new AbilityList();
	
	/**
	 * Returns the newly instantiated instance of the class for use in other classes
	 * @return
	 */
	public static AbilityList getInstance() {
		return instance;
	}

	private String mageClass = "Mage";
	private String iceClass = "Ice";
	private String snowClass = "Snow";
	private String fireClass = "Fire";
	private String creeperClass = "Creeper";
	
	/**
	 * Returns the string value for mage
	 * @return
	 */
	public String getMageClass() {
		return mageClass;
	}
	
	/**
	 * Returns the string value for ice
	 * @return
	 */
	public String getIceClass() {
		return iceClass;
	}
	
	/**
	 * Returns the string value for snow
	 * @return
	 */
	public String getSnowClass() {
		return snowClass;
	}
	
	/**
	 * Returns the string value for fire
	 * @return
	 */
	public String getFireClass() {
		return fireClass;
	}
	
	/**
	 * Returns the string value for creeper
	 * @return
	 */
	public String getCreeperClass() {
		return creeperClass;
	}
}
