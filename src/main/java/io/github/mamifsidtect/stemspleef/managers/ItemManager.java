package io.github.mamifsidtect.stemspleef.managers;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemManager {
	
	private static ItemManager instance = new ItemManager();
	
	public static ItemManager getInstance() {
		return instance;
	}
	
	public ItemStack classWand = new ItemStack(Material.STICK); {
		ItemMeta clWand = classWand.getItemMeta();
		ArrayList<String> clLore = new ArrayList<String>();
		clWand.setDisplayName(ChatColor.YELLOW + "Select Your Ability (Right-Click)");
		
		clLore.add("");
		clLore.add(ChatColor.GOLD + "Select your ability with a swish of your wrist");
		
		clWand.setLore(clLore);
		classWand.setItemMeta(clWand);
	}
	
	public ItemStack creeperWand = new ItemStack(Material.SULPHUR); {
		ItemMeta cWand = creeperWand.getItemMeta();
		ArrayList<String> cLore = new ArrayList<String>();
		cWand.setDisplayName(ChatColor.GREEN + "Creeper Ability Wand (Right-Click");
		
		cLore.add("");
		cLore.add(ChatColor.GOLD + "Someone once told you");
		cLore.add(ChatColor.GOLD + "\"You have a very explosive personality\"...");
		cLore.add(ChatColor.GOLD + "You're going to make them eat their words");
		
		cWand.addEnchant(Enchantment.LUCK, 10, true);
		
		cWand.setLore(cLore);
		creeperWand.setItemMeta(cWand);
	}
	
	public ItemStack iceWand = new ItemStack(Material.PACKED_ICE); {
		ItemMeta iWand = iceWand.getItemMeta();
		ArrayList<String> iLore = new ArrayList<String>();
		iWand.setDisplayName(ChatColor.AQUA + "Ice Ability Wand (Right-Click)");
		
		iLore.add("");
		iLore.add(ChatColor.GOLD + "Your heart is as cold as your class");
		iLore.add(ChatColor.GOLD + "And never let it go.");
		
		iWand.addEnchant(Enchantment.LUCK, 10, true);
		
		iWand.setLore(iLore);
		iceWand.setItemMeta(iWand);
	}
	
	public ItemStack mageWand = new ItemStack(Material.EYE_OF_ENDER); {
		ItemMeta mWand = mageWand.getItemMeta();
		ArrayList<String> mLore = new ArrayList<String>();
		mWand.setDisplayName(ChatColor.DARK_AQUA + "Mage Ability Wand(Right-Click)");
		
		mLore.add("");
		mLore.add(ChatColor.GOLD + "Enchant them with your moves");
		
		mWand.addEnchant(Enchantment.LUCK, 10, true);
		
		mWand.setLore(mLore);
		mageWand.setItemMeta(mWand);
	}
	
	public ItemStack fireWand = new ItemStack(Material.BLAZE_ROD); {
		ItemMeta fWand = fireWand.getItemMeta();
		ArrayList<String> fLore = new ArrayList<String>();
		fWand.setDisplayName(ChatColor.RED + "Fire Ability Wand(Right-Click)");
		
		fLore.add("");
		fLore.add(ChatColor.GOLD + "Burn them! Burn them all!");
		
		fWand.addEnchant(Enchantment.LUCK, 10, true);
		
		fWand.setLore(fLore);
		fireWand.setItemMeta(fWand);
	}
	
	public ItemStack snowWand = new ItemStack(Material.SNOW_BALL); {
		ItemMeta snWand = snowWand.getItemMeta();
		ArrayList<String> snLore = new ArrayList<String>();
		snWand.setDisplayName("Snow Ability Wand (Right-Click)");
		
		snLore.add("");
		snLore.add(ChatColor.GOLD + "Only with your powers can everyone survive");
		
		snWand.addEnchant(Enchantment.LUCK, 10, true);
		
		snWand.setLore(snLore);
		snowWand.setItemMeta(snWand);
	}
	
	public void giveClassWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(classWand));
	}
	
	public void giveCreeperWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(creeperWand));
	}
	
	public void giveIceWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(iceWand));
	}
	
	public void giveMageWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(mageWand));
	}
	
	public void giveFireWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(fireWand));
	}
	
	public void giveSnowWand(Player p, Integer inventorySlot) {
		p.getInventory().setItem(inventorySlot, new ItemStack(snowWand));
	}
}
