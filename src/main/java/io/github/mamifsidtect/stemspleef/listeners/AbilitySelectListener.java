package io.github.mamifsidtect.stemspleef.listeners;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.managers.AbilityList;
import io.github.mamifsidtect.stemspleef.managers.ItemManager;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class AbilitySelectListener implements Listener {

	public static ItemManager im = new ItemManager();
	
	/**
	 * Just opens the ability menu when 
	 * holding the ability select stick
	 * @param event
	 */
	@EventHandler
	public void onAbilityStickClick(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		Action a = event.getAction();
		if (p.getItemInHand().equals(im.classWand)) {
			if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
				p.openInventory(StemSpleef.abilityMenu);
			}
		}
	}
	
	/**
	 * Adds player name to hashmap of abilities
	 * Specifically the ice string
	 * @param event
	 */
	@EventHandler
	public void onIceSelect(InventoryClickEvent event) {
		HumanEntity p = event.getWhoClicked();
		if (event.getInventory() == StemSpleef.abilityMenu) {
			if (event.getCurrentItem() == im.iceWand) {
				if (!AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getIceClass());
					event.setCancelled(true);
					p.closeInventory();
				} else {
					AbilityListener.getInstance().getPlayerAblility().remove(p.getName());
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getIceClass());
					event.setCancelled(true);
					p.closeInventory();
				}
			}
		}
	}
	
	/**
	 * Adds player name to hashmap of abilities
	 * Specifically the creeper string
	 * @param event
	 */
	@EventHandler
	public void onCreeperSelect(InventoryClickEvent event) {
		HumanEntity p = event.getWhoClicked();
		if (event.getInventory() == StemSpleef.abilityMenu) {
			if (event.getCurrentItem() == im.creeperWand) {
				if (!AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getCreeperClass());
					event.setCancelled(true);
					p.closeInventory();
				} else {
					AbilityListener.getInstance().getPlayerAblility().remove(p.getName());
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getCreeperClass());
					event.setCancelled(true);
					p.closeInventory();
				}
			}
		}
	}
	
	/**
	 * Adds player name to hashmap of abilities
	 * Specifically the fire string
	 * @param event
	 */
	@EventHandler
	public void onFireSelect(InventoryClickEvent event) {
		HumanEntity p = event.getWhoClicked();
		if (event.getInventory() == StemSpleef.abilityMenu) {
			if (event.getCurrentItem() == im.fireWand) {
				if (!AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getFireClass());
					event.setCancelled(true);
					p.closeInventory();
				} else {
					AbilityListener.getInstance().getPlayerAblility().remove(p.getName());
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getFireClass());
					event.setCancelled(true);
					p.closeInventory();
				}
			}
		}
	}
	
	/**
	 * Adds player name to hashmap of abilities
	 * Specifically the mage string
	 * @param event
	 */
	@EventHandler
	public void onMageSelect(InventoryClickEvent event) {
		HumanEntity p = event.getWhoClicked();
		if (event.getInventory() == StemSpleef.abilityMenu) {
			if (event.getCurrentItem() == im.mageWand) {
				if (!AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getMageClass());
					event.setCancelled(true);
					p.closeInventory();
				} else {
					AbilityListener.getInstance().getPlayerAblility().remove(p.getName());
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getMageClass());
					event.setCancelled(true);
					p.closeInventory();
				}
			}
		}
	}
	
	/**
	 * Adds player name to hashmap of abilities
	 * Specifically the ice string
	 * @param event
	 */
	@EventHandler
	public void onSnowSelect(InventoryClickEvent event) {
		HumanEntity p = event.getWhoClicked();
		if (event.getInventory() == StemSpleef.abilityMenu) {
			if (event.getCurrentItem() == im.snowWand) {
				if (!AbilityListener.getInstance().getPlayerAblility().containsKey(p.getName())) {
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getSnowClass());
					event.setCancelled(true);
					p.closeInventory();
				} else {
					AbilityListener.getInstance().getPlayerAblility().remove(p.getName());
					AbilityListener.getInstance().getPlayerAblility().put(p.getName(), AbilityList.getInstance().getSnowClass());
					event.setCancelled(true);
					p.closeInventory();
				}
			}
		}
	}
}
