package io.github.mamifsidtect.stemspleef.listeners;

import io.github.mamifsidtect.stemspleef.managers.Arena.ArenaState;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class GameListener implements Listener {

	private static GameListener instance = new GameListener();

	public static GameListener getInstance() {
		return instance;
	}
	
	private static HashMap<String, Integer> blocksBroken = new HashMap<String, Integer>();
	
	public HashMap<String, Integer> getBlocksBroken() {
		return blocksBroken;
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		if (event.getEntity().getType() == EntityType.PLAYER) {
			Player p = (Player) event.getEntity();
			if (ArenaManager.getInstance().getArena(p) != null) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerLavaStep(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		Block bUnder = p.getLocation().getBlock().getRelative(BlockFace.DOWN);
		Block bStandingIn = p.getLocation().getBlock().getRelative(BlockFace.SELF);
		
		if (ArenaManager.getInstance().getArena(p) == null) {
			return;
		}
		
		if (bUnder.getType() != Material.LAVA 
				|| bUnder.getType() != Material.STATIONARY_LAVA 
				|| bStandingIn.getType() != Material.LAVA 
				|| bStandingIn.getType() != Material.STATIONARY_LAVA) {
			return;
		}
		
		ArenaManager.getInstance().getArena(p).removePlayer(p);

		MessageManager.getInstance().msg(p, MessageType.INFO, "You have died! And are now a spectator (coming soon)");
	}
	
	@EventHandler
	public void onPlayerBreak(PlayerInteractEvent event) {
		Block b = event.getClickedBlock();
		Player p = event.getPlayer();
		Action a = event.getAction();
		
		if (ArenaManager.getInstance().getArena(p) != null) {
			if (ArenaManager.getInstance().getArena(p).getState() != ArenaState.SPLEEFING) {
				if (a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK) {
					event.setCancelled(true);
				}
			} else {
				if (p.getItemInHand().getType() == Material.DIAMOND_SPADE) {
					if (a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK) {
						event.setCancelled(true);
						b.setType(Material.AIR);
						b.getDrops().clear();
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Entity e = event.getEntity();
		if (e instanceof Player) {
			event.setDeathMessage(null);
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if (event.getEntity().getType() == EntityType.PLAYER) {
			Player p = (Player) event.getEntity();
			event.setCancelled(true);
			p.setFoodLevel(20);
		}
	}
}
