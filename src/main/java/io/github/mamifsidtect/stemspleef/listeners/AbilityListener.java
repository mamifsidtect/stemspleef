package io.github.mamifsidtect.stemspleef.listeners;

import java.util.HashMap;

import org.bukkit.event.Listener;

public class AbilityListener implements Listener {

	private static AbilityListener instance = new AbilityListener();
	
	public static AbilityListener getInstance() {
		return instance;
	}
	
	public HashMap<String, String> playerAbility = new HashMap<String, String>();
	
	public HashMap<String, String> getPlayerAblility() {
		return playerAbility;
	}
}
