package io.github.mamifsidtect.stemspleef.commands;

import io.github.mamifsidtect.stemspleef.managers.Arena.ArenaState;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Leave extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (ArenaManager.getInstance().getArena(p) == null) {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You are not already in an arena!");
			return;
		}
		
		if (ArenaManager.getInstance().getArena(p).getState() != ArenaState.SPLEEFING) {
			ArenaManager.getInstance().getArena(p).removePlayer(p);
			return;
		}
		
		if (ArenaManager.getInstance().getArena(p).getState() == ArenaState.SPLEEFING) {
			ArenaManager.getInstance().getArena(p).playerLeave(p);
			return;
		}
	}

	public Leave() {
		super("Leave an arena.", "", "leave");
	}
}