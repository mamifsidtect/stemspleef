package io.github.mamifsidtect.stemspleef.commands;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Reload extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (StemSpleef.perms.playerHas(p, "goldengun.commands.reload")) {
			ArenaManager.getInstance().setupArenas();
			MessageManager.getInstance().msg(p, MessageType.GOOD, "Reloaded goldengun!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public Reload() {
		super("Reload the arenas.", "", "reload");
	}
}