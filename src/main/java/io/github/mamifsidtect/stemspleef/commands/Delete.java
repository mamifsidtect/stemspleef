package io.github.mamifsidtect.stemspleef.commands;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.managers.Arena;
import io.github.mamifsidtect.stemspleef.managers.Arena.ArenaState;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.SettingsManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Delete extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (StemSpleef.perms.playerHas(p, "goldengun.commands.delete")) {
			if (args.length == 0) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You must specify an arena number!");
				return;
			}

			int id = -1;

			try { id = Integer.parseInt(args[0]); }
			catch (Exception e) {
				MessageManager.getInstance().msg(p, MessageType.BAD, args[0] + " is not a valid number!");
				return;
			}

			Arena a = ArenaManager.getInstance().getArena(id);

			if (a == null) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "There is no arena with ID " + id + "!");
				return;
			}

			if (a.getState() == ArenaState.SPLEEFING) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "Arena " + id + " is ingame!");
				return;
			}

			SettingsManager.getArenas().set("arenas." + id + "", null);

			ArenaManager.getInstance().setupArenas();

			MessageManager.getInstance().msg(p, MessageType.GOOD, "Deleted arena " + id + "!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public Delete() {
		super("Delete an arena.", "<id>", "delete");
	}
}