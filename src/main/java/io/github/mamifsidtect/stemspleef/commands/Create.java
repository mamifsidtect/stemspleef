package io.github.mamifsidtect.stemspleef.commands;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.SettingsManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import org.bukkit.entity.Player;

public class Create extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (StemSpleef.perms.has(p, "goldengun.commands.create")) {
			int id = ArenaManager.getInstance().getArenas().size() + 1;

			SettingsManager.getArenas().createConfigurationSection("arenas." + id);
			SettingsManager.getArenas().set("arenas." + id + ".numPlayers", 10);
			SettingsManager.getArenas().set("arenas." + id + ".minPlayers", 4);
			SettingsManager.getArenas().set("arenas." + id + ".mapName", "Default Name");

			MessageManager.getInstance().msg(p, MessageType.GOOD, "Created Arena " + id + "!");

			ArenaManager.getInstance().setupArenas();
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}

	public Create() {
		super("Create an arena, ", "", "create");
	}
}