package io.github.mamifsidtect.stemspleef.commands;

import io.github.mamifsidtect.stemspleef.StemSpleef;
import io.github.mamifsidtect.stemspleef.managers.ArenaManager;
import io.github.mamifsidtect.stemspleef.managers.SettingsManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager;
import io.github.mamifsidtect.stemspleef.managers.MessageManager.MessageType;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class SetGameSpawns extends MagicCommands {

	public void onCommand(Player p, String[] args) {
		if (StemSpleef.perms.playerHas(p, "goldengun.commands.setspawns")) {
			if (args.length == 0) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You did not specify an arena ID or a respawn point ID!");
				return;
			} else if (args.length == 1) {
				MessageManager.getInstance().msg(p, MessageType.BAD, "You did not specify a respawn point ID!");
				return;
			}
			
			int id = -1;
			int spawnID = -1;
			
			try { id = Integer.parseInt(args[0]); }
			catch (Exception e) { 
				MessageManager.getInstance().msg(p, MessageType.BAD, args[0] + " is not a valid number!");
				return;
			}
			
			try { spawnID = Integer.parseInt(args[1]); }
			catch (Exception e) {
				MessageManager.getInstance().msg(p, MessageType.BAD, args[1] + " is not a valid number for respawn point!");
				return;
			}
			
			ConfigurationSection s = SettingsManager.getArenas().createConfigurationSection("arenas." + id + ".respawns." + spawnID);
			
			s.set("world", p.getWorld().getName());
			s.set("x", p.getLocation().getX());
			s.set("y", p.getLocation().getY());
			s.set("z", p.getLocation().getY());
			s.set("yaw", p.getLocation().getYaw());
			s.set("pitch", p.getLocation().getPitch());
			
			SettingsManager.getArenas().set("arenas." + id + ".respawns." + spawnID, s);
			
			ArenaManager.getInstance().setupArenas();
			
			MessageManager.getInstance().msg(p, MessageType.GOOD, "Set respawn number " + spawnID + " successfully!");
		} else {
			MessageManager.getInstance().msg(p, MessageType.BAD, "You cannot do this!");
		}
	}
	
	public SetGameSpawns() {
		super("Sets the spawn of the number that you specify in an arena.", "<arenaID> <spawnID>", "sgs");
	}
}
