package io.github.mamifsidtect.stemspleef.tools;

public interface TypedRunnable<T> {
    public void run(T o);
}
